---
title: "Show and Tell"
---

## Show And Tell

* Show incomplete work
* Solicit feedback
* Change direction based on feedback
* You are not your code