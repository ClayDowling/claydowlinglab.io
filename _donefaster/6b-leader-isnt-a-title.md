---
title: "Leader Isnt' a Title"
---

<blockquote>
    &ldquo;Leader isn't a title, it's a way of life.&rdquo;
    <br />
    <cite>&mdash;John Gartee</cite>
</blockquote>