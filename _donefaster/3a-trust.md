---
title: "Trust"
---

# Trust

A team that trusts each other can move mountains. A team that lacks trust can barely roll down hill.