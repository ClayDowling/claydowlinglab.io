---
title: "You're Going to Eff Up"
---

## You're Going To Build The Wrong Thing

Make changing direction cheap.

* Invest the bare minimum in planning.
* Keep backlogs small.
* Craftsmanship is important.
* Don't prematurely optimize.
