---
title: "Bibliography"
---

### Bibliography

- [Journey to Enterprise Agility](https://amzn.to/2qByI6T)
- [Leadership and Self Deception](https://amzn.to/2z0FYxz)
- [Getting Naked](https://amzn.to/2FeEd5L)

## Training

[Cream of the Crop Leadership Development](https://creamofthecropleaders.com/)