---
title: "Short Feedback Loops"
---

# Short Feedback Loops

Mistakes caught early are cheaper than mistakes caught late.