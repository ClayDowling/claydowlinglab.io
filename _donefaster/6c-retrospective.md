---
title: "Retrospective"
---

# Retrospective

Talk about what worked well. Celebrate victories.

Talk about what didn't work well. Take action to fix it.