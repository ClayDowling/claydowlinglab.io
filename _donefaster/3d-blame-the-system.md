---
title: "Blame the System"
---

# Blame The System, Not The Person

Seek the flaw in the system, not the person.

From their own perspective, every person is a rational actor.