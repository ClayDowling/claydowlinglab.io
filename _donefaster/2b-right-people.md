---
title: "The Right People"
---
## The Right People

The smartest person isn't necessarily the best.

Adaptability is more useful than skill in a technology or technique.