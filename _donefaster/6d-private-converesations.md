---
title: "Private Conversations"
---

# Private Conversations

Some problems are hard to talk about in a group. Try to talk to everyone privately as well.