---
title: "Speak the Kind Truth"
---

# Speak the Kind Truth

When there is a problem, do not avoid it.

Have empathy for the person you are addressing.