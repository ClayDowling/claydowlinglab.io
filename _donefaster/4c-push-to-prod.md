---
title: "Push to Production Every Week"
---
## Push to Production Every Week

* Deploy to production in week 1.
* Deploy as often as you can.
* Remove barriers relentlessly.