---
title: "Maximize the Work Not Done"
---

## Maximize The Work Not Done

* Focus on value delivered, not features
* Ruthlessly cut features as you achieve value
* Celebrate the work you won't have to do