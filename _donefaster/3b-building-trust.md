---
title: "Building Trust"
---

# Building Trust

Be open about your failings and weaknesses.

Encourage and act on feedback.

Extend trust to others without expectation.