---
title: "Fix Your Problems"
---

# Fix Your Problems

Sometimes it's easier to ask for forgiveness than permission.