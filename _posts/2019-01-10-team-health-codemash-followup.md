---
title: "Team Health for Fun and Profit - CodeMash 2019 Followup Edition"
tags: codemash
---

In my talk [Team Health for Fun and Profit](/teamhealth) I strongly advocate
for working reasonable hours.  The 40 hour work week was paid for in blood, and
we ignore it at our peril.  A person in the audience pointed out that it's
common in the tech industry for somebody who only puts in their 40 to be
perceived as a "clock puncher."  Somebody with no passion for the business, not
as dedicated to the goal as everyone else.

My entire talk is basically about throwing off that attitude, but his point is
legitimate: no matter how much you may internalize the importance of limiting
your work week to stay at your best, the people around you still have these old
attitudes, and that *will* have an impact on your career.


[Kim Arnett](https://twitter.com/kaydacode) expressed a solution that I think is
probably the most elegant that I have heard: Don't be a part of the problem.
Agressively invite coworkers out to lunch.  Encourage your coworkers to leave
after eight hours.  Make sure that you leave after eight hours.  Do what [Ronda
Bergman](https://twitter.com/rabergman) calls "Leave Loud."  

This isn't to say that I'm advocating for slacking off at work.  I'm advocating
for a good productive eight hours.  That's actually the point of limiting the
work day.  I want to be fresh so I can focus, so I support my team and don't
drag them down.  I want to be happy to be there, making a positive impact on
the lives of my coworkers and the people who use my software.  That means
setting boundaries for how much of my life the job can have.

## The Money

All that touchey-feeling noise doesn't carry much weight with management, unless
you have the fortune to work for very enlightened people (and by enlightened, I
mean that they agree with me).  You need to talk about something they care about.

Money is the common language of any business.  Fortunately the money supports
my position.  If I'm writing code for 50 hours, chances are pretty good that 10
hours of that work aren't my best work.  I'll probably have to redo it.  The
sneaky part is that it won't be obvious that I need to redo it until later,
possibly months later.

What that means is that I'm throwing out 10 hours of work.  Money that was paid
to me, that turns out to be money wasted.  And I need to put in another 10
hours to redo the work.  So now the business is paying me twice for the same
amount of final work product.  There's an additional opportunity cost: during
that 10 hours, I was going to be producing some different bit of business
value.  That's been pushed out now.  Whatever that bit was worth to the
company, they aren't getting it now, because I'm fixing my old work.

From a business perspective, long hours are really expensive.  It's a hidden
cost though: on the surface it looked like free labor, and the business
definitely loves free labor.  They're likely to treat the people who work long
hours as heroes.  By the time the true costs is realized, it's likely they
don't associate it with the free benefit they got months ago.

## Deeper Costs to Long Hours

If your developers are writing software with problems, those problems are
likely to be directly or indirectly visible to customers.  Customers give us
money to pay our bills and fund our expensive habits.  At one client, after
months of nights and weekends, they put the software into production.  In the
first month they saw $600,000 of sales lost to transactions hung up in their
new sales system.  That doesn't include the sales lost because the customers
couldn't even start the sales process.  Or future sales lost because formerly
loyal customers, who needed the product now, went to a competitor to get it.

Another cost is employee burnout.  Employees who are continually over
stretching themselves will burn out.  No matter how tough they tell themselves
they are, they burn out.  They'll either leave of their own accord, or you'll
have to show them the door because they can't do the job any more.  Hiring
developers is a long and expensive process.  Even once you get a new developer
in, it takes months to make them fully productive members of a team.  No matter
how senior you are, you still need to learn the domain and the specifics of the
problem that you're solving with the software.

At the customer I mentioned above, the company paid out nice performance
bonuses to employees once the product was delivered.  As soon as those bonuses
were paid out, email became a steady stream of "It's been great working with
all of you wonderful people.  I'm moving on to other opportunities at our
competitors."  From a strictly financial perspective, each person walking out
the door for the last time represented a large pile of money invested, and
another large pile that the company would need to spend to replace them.

## The Bottom Line

The bottom line is that working long hours is bad for the bottom line.  It's
the equivalent of running traditional factory machinery without doing
preventative maintenance (for the non-machinists reading this: preventive
maintenance can be the difference between financial ruin and wild profits for a
factory).

Be fiscally responsible: punch the clock, work shorter hours, and keep your
productivity high.
