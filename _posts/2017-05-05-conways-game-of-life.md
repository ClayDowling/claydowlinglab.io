---
title: "A Functional Conway's Game of Life"
categories: development
---

Conway's Game of Life is an old standby for code retreats and coding katas.  The game's simplicity means that implementation complexity won't get in the way of what you are trying to learn.

A few months back in a code retreat a fellow attendee implemented the game in a functional language.  My first thought was "why would anybody do that?"  My second was "how could I do this in C?"  C isn't normally thought of as a functional language, but there's nothing stopping it from being used that way.  Functions have always been first class objects, and applying a function to a collection of objects is trivial.

I put the thought away for a while, but recently I decided that building this as an LED array driven by a microprocessor would be fun.  So I pulled the project out of the vault and started working on it during my lunch hour with my colleagues.

You can see the results on my [Functional Life] (https://github.com/ClayDowling/functional-life) github repo.

## How is This Functional?

While I am treating functions as first class objects, and applying them to an entire dataset, I'm not generating immutable data sets.  Doing so would suggest a lot of memory allocation, and memory allocation isn't really a thing in embedded systems.

Instead I have a pair of grids.  I read from one and write to the other.  When one is being read the other is being written.  It's the closest I can come to immutability.

## What Did I Learn?

Like any craftsman, I can see plenty of flaws in my work.

But I also liked the simplicity of being able to apply my function to a full data set with a single line of code.  This is a very seductive way to write software.  I'm not racing out to abandon my old established practices, but I am going to look for places to apply the techniques in my work for clients.



{{tag>craftsmanship, coding, functional, C}}



