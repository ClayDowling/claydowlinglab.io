---
title: "Meritocracy and Other Comfortable Lies"
---

In tech we love to tell ourselves that we operate on merit.  It's nice to tell ourselves that we work at a place that only hires the best.  Out of everyone else who tried to get my job, I was the best.  And that is true, I was the best of the people who applied for my job.  The problem is that the best way to lie is to use the truth to do it.

If you've ever tried to hire for a tech job, you know that the biggest problem is finding someone to apply for the job. So yes, I'm better than everyone else who applied for this job.  But nobody else applied.  I'm unique and special, like everybody else.

My employer was recently bought by a global consulting firm.  We had our kickoff event yesterday, and a well meaning executive describe to company as a meritocracy.  Promotions and raises are based on merit.  Only the best rise. Only the best get the choice assignments.  Within a minute of saying that, the poor gentleman was sorry he even knew the word meritocracy.

The women in the room called him on the fact that mertitocracies disadvantage women and ethnic minorities.  Companies are eager to avoid the appearance of discriminating against women and ethnic minorities. This is a great approach to take when addressing corporate culture.

I'm a middle aged cis hetero white dude.  The system was made for me.  I still don't want to work in a meritocracy.  Meritocracies create a toxic culture.  Only the best get the good assignments. Only the best get the promotions. Only the best get the raises.  I'm always competing against everyone I work with to be better than them.  That makes my professional life one big, unending dick measuring contest.  I've had those jobs, and I did't like it.  I'm much happier working someplace where that isn't the culture.

Most tech jobs are filled by somebody who knew somebody in the company.  If I refer somebody, I'm going to have to work with them.  So I only refer people I want to work with, not against.  I've worked both ways, and "with" is much more productive and better for my happiness than "against."

Next time someone describes an organization as a meritocracy, ask yourself if you want to work there.  Ask yourself if you want to compete against everyone around you, or if you'd rather work together to do something cool.  I'd rather do the cool thing.