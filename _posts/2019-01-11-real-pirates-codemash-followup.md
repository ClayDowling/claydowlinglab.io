---
title: "Real Pirates Seek the C! CodeMash Follow Up Edition"
---

I was really happy to see all the people who cam out and wanted to hear about C programming at CodeMash.  It's wonderful to find a place with so many people who share my same obscure fascination.

## Books

I neglected to put a bibliography into my slides, so let me address that here.

### Learning C

The gold standard is [The C Programming Language](https://amzn.to/2AJa0XP), but the prices offered on Amazon on ridiculously high.

You should definitely check out [21st Century C: Tips from the New School](https://amzn.to/2Fx8Gds) to learn about the wonderful changes that have come to the language since 1988.

You'll also need a compiler and associated stanard libraries and headers.  GCC is ubiquitous, especially on Unix style systems.  

* On an ubuntu style system you can get it by `apt install dev-essentials` and you'll have everything you need.
* On Windows, the easiest way to get GCC is to install [MinGW64](https://mingw-w64.org/doku.php).

Earlier experiences with LLVM (a.k.a. clang) were a mixed bag, but I just installed it on my Windows laptop, and it's great.  If you're working in Windows, it's worth giving [LLVM](http://releases.llvm.org/download.html) a try.

### C Tools

If you're just learning C, you'll need an editor of some kind.  Until a few years ago, [Vim](https://www.vim.org/) and [Emacs](https://www.gnu.org/software/emacs/) were state of the art for code editors.  For somebody just starting out, I'd suggest [Atom](https://atom.io/) or [VS Code](https://code.visualstudio.com/) with C/C++ plugins.  I'm still a vi guy, but VS Code is my day to day goto.

If you really must have an IDE, I strongly recommend [QtCreator](https://www.qt.io/qt-features-libraries-apis-tools-and-ide/) or [CLion](https://www.jetbrains.com/clion/).  I own and love both, but it's honestly rare for me to use an IDE.

## Other Uses for C

### Parsers

If you're willing to get a bit meta, you can design your own domain specific language.  You don't need to design a full on programming language to get value from domain specific languages.  You could just as easily do it if you have an application which needs a more complex configuration than just setting key value pairs in a config file.

There's a lot written about Domain Specific Languages, but the long and short of it is that they let your express your problem and the solution more concisely, which lets you solve your problem faster.

Writing parsers and lexers is a problem that's been solved for nearly as long as C has been around.  These tools let you write your own domain specific language quickly and easily.  [Bison](https://www.gnu.org/software/bison/) is a tool that you use to implement the grammar of your language, putting symbols into the correct relation to each other.  [Flex](https://github.com/westes/flex) is a lexer, which takes your source code from a text file, and converts it to a stream of symbols for the parser to use.

Writing your own domain specific language is a relatively advanced topic.  I was first introduced to this world by a simple exercise to convert infix mathematical notation (e.g. 4 * 5 + 6) to postfix notation (4 5 * 6 +), and a friend's recommendation that I investigate [Abstract Syntax Trees](https://en.wikipedia.org/wiki/Abstract_syntax_tree).  I had a lot of fun implementing that and I've had an itch ever since to solve more problems with parsers.

### Games

A lot of people came to programming through a love of games, and games are a great reason to explore C programming.  I'm not a game developer, but I have walked through simple tutorials for [Simple Direct Media Layer](https://www.libsdl.org/).  The library seemed pretty easy to use, and if you want to try game programming this is as good a place as any to start.