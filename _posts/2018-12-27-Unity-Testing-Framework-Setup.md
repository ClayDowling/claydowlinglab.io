---
title: "Setting Up the Unity Testing Framework"
categories: development
---

I've been drinking the Test First kool-aide for years.  So when I write
embedded code for an Arduino, I want to test that code before pushing it to my
lovingly hand-wired circuit.  C doesn't get a lot of love in the testing world,
which is a shame.  It's probably easier to write automated tests for C than any
other language.

One of my favorite tools for light weight testing is
[Unity](https://github.com/ThrowTheSwitch/Unity) from [Throw the
Switch](http://www.throwtheswitch.org/).  In it's default form it offers good
testing capabilities.  The source repository has an "extras" folder which
supports a more full-featured testing framework.  Here's my recipe for adding
the full framework to a project.

To see how this fits into the larger use case of project setup for test driven
embedded code, have a read of [Makefiles for Test Driven
Code]({% post_url _posts/2017-12-15-Makefiles-for-Test-Driven-Development %}).

## Assembling the Source

I clone the source repo (or update it) on my workstation.

In my project, under the project root folder, I add a "unity" folder, which
we'll call the *library* folder for purposes of our discussion.

From the unity repo, copy the contents of the "src" folder to the library folder.

From the unity repo, copy the contents of the "extra/fixture/src" folder to the
library folder.

## Build the Library

In the unity library folder, add the following Makefile:

    # unity/Makefile
    
    LIBNAME=unity

    SRC=$(wildcard *.c)
    BASES=$(basename $(SRC))
    OBJS=$(addsuffix .o, $(BASES))

    lib$(LIBNAME).a: $(OBJS)
        ar -rs $@ $^

    clean:
        rm -rf *.o
        rm -rf *.a

By default, running make in this folder will build a file libunity.a, which
will include all of the files from the library.
