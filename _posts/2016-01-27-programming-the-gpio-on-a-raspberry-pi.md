---
title: "Programming the GPIO on a Raspberry Pi"
tags: embedded programming
categories: development
---

If you read my previous piece on putting FreeBSD on a Raspberry Pi, you're probably now wondering what exactly you're going to do with this little box.  Fortunately I have invested the effort to work out some basics of handling GPIO on FreeBSD and a Raspberry Pi.  Because of why I chose FreeBSD, I've also implemented it in C: my first choice for tackling new problems in systems programming.

## Setup

You can, of course, connect to the box with a keyboard and screen.  But years of systems programming have taught me that what I really want is a whole lot of terminals open and connected to the target system.  To that end I worked out what my Pi's network address was and used SSH to connect from my laptop.  Because I got tired very quickly of typing in passwords, I put my SSH public key in ~baker/.ssh/known_keys file on the Pi.

Since we're playing with the external interface, you'll also need a breadboard, the electronics components you want to experiment with, and to make things happen more quickly, a [Pi Cobbler](http://amzn.to/1lU6VcF).

You'll also want your development tools.  This is low level systems work, so your IDE looks a lot like a few terminals and your favorite editor.  By default the Pi has vi installed on it, but this is old nasty vi, the stuff I learned on back in the dark ages.  You'll probably want vim, or if you're younger, pico.

This tutorial also requires cmake, because I didn't feel like messing around with BSD make files.  BSD make is just enough different than GNU make that it trips me up.  You won't have it installed by default, so make sure you install it.

## Introducing Ourselves to the GPIO Bus

The very first thing I needed to do was make sure I could talk to the GPIO bus at all, so a logic step was to query it.

```C
/**
 * 
 * LED blinker program
 */
#include <inttypes.h>
#include <stdlib.h>
#include <libgpio.h>
#include <stdio.h>

/* char* gpio_flag(uint32_t); */

int main(int argc, char **argv)
{
	gpio_handle_t hdl;
	gpio_config_t* pcfgs = NULL;
	gpio_config_t config;
	int i;
	int maxpin;

	hdl = gpio_open(0);
        if (hdl == -1) {
                perror("gpio");
                return EXIT_FAILURE;
        }
	maxpin = gpio_pin_list(hdl, &pcfgs);
	printf("%d pins\n", maxpin);
	for(i=0; i < maxpin; ++i) {
		config = pcfgs[i];
		printf("%d] \"%s\"\n", config.g_pin,
			config.g_name);
	}

	gpio_close(hdl);

	return EXIT_SUCCESS;
}
```

You'll also need to edit a file CMakeLists.txt:

```cmake
cmake_minimum_required(VERSION 2.8)
project(gpiospike)

add_executable(gpiolist
	gpiolist.c
	)
target_link_libraries(gpiolist
	gpio
	)
```

Now create a folder under your working folder named "build" and cd to that folder.  Then run cmake on the file:

	mkdir build
	cmake ..
  
This should cause a good deal of activity.  If there are errors, resolve them and repeat the cmake cycle until you wind up with a Makefile in the build folder.

Now run "make" and you should get nice output showing a successfully built program called gpiolist.  You can run it now.  This is what happened for me:

	[baker@rpi2 ~/gpio/build]$ ./gpiolist
	gpio: Permission denied
  
The GPIO bus is a low level system resource, and FreeBSD very wisely doesn't give you access to that.  You'll need to be root.  If you're used to Linux, you're probably expecting to issue a command like "sudo ./gpiolist"  That won't work here, because FreeBSD doesn't have sudo.  Instead you'll need to go old school:

  	su -
  
Now you'll need to change to the build folder, which on my system was:

  	cd ~baker/gpio/build

Now you should get a nice screen listing that will end list this:

	45] "pin 45"
	46] "pin 46"
	47] "gpioled1"
	48] "pin 48"
	49] "pin 49"
	50] "pin 50"
	51] "pin 51"
	52] "pin 52"
  
It's nothing stellar, but it's a start.
