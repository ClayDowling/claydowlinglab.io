---
title: "Clay's Pie Crust Recipe"
---

{{:blog:blueberry-pie.jpg?direct&200 |}}I've cobbled this recipe together from various sources, starting with my
grandmother's pie crust recipe, and adding in things I learned from my
mother and reading cooking magazines.

## Tools

This recipe requires a kitchen scale capable of measuring in grams.  It's
important to keep the ratios of liquids to solids correct, or you'll have a
bad time.  Learn from my pain.

## Stage 1

| Ingredient  | grams | Cumulative grams |
|-------------|-------|------------------|
| Flour       | 180   |                  |
| Salt        | 1     | 181              |
| Lard        | 102   | 283              |

Measure the ingredients into a bowl on the scale, looking for the cumulative balance total on your scale for each step.  The proportion of lard to flour is important for the flakiness and texture of the final product.

Cut the lard into the flour with a fork or a pastry cutter until clumps roughly the size of rice are formed.  The clumps of flour around fat help to form the flakes of the crust.

## Stage 2

| Ingredient  | grams | Cumulative grams |
|-------------|-------|------------------|
| Egg (large) | 50    |                  |
| Water       | 30    | 80               |
| Vodka       | 27    | 107              |

Measure the liquid ingredients into another bowl or measuring cup on top of
the scale.  The final mass of 107g is much more important than the measure
of the individual ingredients.

Mix the liquid ingredients into the flour/lard mixture. The result should be a bit pebbly and uneven.  Do not overwork the dough, as this leads to the formation of gluten strands and an armor plated crust.

The vodka does not need to be especially good, so long as it does not have a distinctive flavor.  It is present to provide a liquid for mixing, but the alcohol retards formation of gluten strands.

## Preparation of the Crust

This dough is very soft and must be chilled to be workable.  At least half an hour in the refrigerator will be necessary before you can work it. An hour is better.  This is a great time to prepare your filling.

The dough should be rolled between sheets of wax paper until it is the desired thickness.  Using a floured board will be harder and reduce the flakiness of the crust.

## Baking

### For a Fruit Filled Pie

Place a cookie sheet on the bottom rack of the oven and preheat to 500 degrees.

When the over is to temperature, put the filled pie on the sheet and reduce the temperature to 425 degrees.  Bake for 25 minutes.
 
Rotate the cookie sheet and reduce the oven temperature to 375 degrees. Continue to bake until juices are bubbling an the crust is deep golden brown, 30-35 minutes longer.

A Note On Fillings: from hard experience, I have learned that thickening the fruit is a challenge.  If you are using corn starch, you'll need to precook the fruit until it thickens.  You can use tapioca or potato starch mixed with the fruit and it will thicken properly during the baking with a pre-cook.  

### For an Open Faced Pie

Before filling, cover the pie crust completely with foil and weight it down with pie weights or pennies.

Bake at 350 for 25-30 minutes.  Carefully remove the weights and allow to cool for a partially baked crust.  For a fully baked crust, continue to bake for 12 minutes.