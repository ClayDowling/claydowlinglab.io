---
title: "FreeBSD on a Raspberry PI"
tags: embedded programming
categories: development
---

Over lunch a colleague mentioned that she wanted to start using Raspberry PI projects to draw crowds at some of our community open house events.  Because I first learned systems programming on FreeBSD, I wanted to try making it work.  Fortunately there's a great little project called [crochet](https://github.com/freebsd/crochet|crochet) that helps you build an image you can burn onto an SD card.  Since cross compiling is actually kind of hard to set up, whenever it encounters a problem it tells you how to fix it.

If you're looking for the quick solution, download this [pre-built image](https://drive.google.com/file/d/0Bzco-26kJXDtcnNWV3Y1OWxsZVE/view?usp=sharing) and burn it to an 8GB SD card.

## Rolling Your Own

The very first thing you need to do is set up a [FreeBSD](http://www.freebsd.org) machine to launch your build from.  You'll want to put it on reasonable hardware, because you'll be compiling the full system (*make world* in FreeBSD parlance).  I run mine on a virtual machine on my Linux laptop.

### Preliminaries

Next you'll need to install crochet on the FreeBSD machine.  If you've just installed your FreeBSD machine (which you probably did), you'll also need to install git:

    pkg add git
  
Then clone the git repo:

    git clone https://github.com/freebsd/crochet.git

You'll also need to install the FreeBSD source.  You can install that off your installation media, or you can install subversion and grab the current source (which is what I did).  You should be aware that FreeBSD comes with something called svnlite, which claims to work just like subversion.  It does in that it recognizes many of the same commands, but it doesn't actually do all the same things.  So install full subversion:

    pkg add subversion
  
Now grab the latest sources for your version of FreeBSD:

    cd /usr/src
    svn co https://svn0.us-west.freebsd.org/base/head
  
The exact host might be different depending on where you live.  I live in the U.S. midwest, so this is what is the recommended host for me.

### Crocheting a New Build

There's a great page that walks you through building an image in the Crochet git repo.  I strongly encourage you to read it.

Here's what I did for mine.  This might or might not work for you.  I'm building for a Raspberry Pi 2 with an 8GB SD card.

Crochet needs a config file.  In the folder you cloned earlier, copy the file config.sh.sample to rp2.cfg.  Here's the diff between the two (click the file name to download and save yourself some typing):

```diff
34c34
< #board_setup RaspberryPi2
---
> board_setup RaspberryPi2
55a56
> option ImageSize 7800mb
115c116
< #option User <username>
---
> option User baker
126c127
< #option SwapFile <size> [deferred] [file=/swapfile0]
---
> option SwapFile 768mb file=/swapfile0
131c132
< #option UsrPorts
---
> option UsrPorts
138c139
< #option UsrPorts <path>
---
> option UsrPorts /usr/ports
160c161
< #option Ntpd
---
> option Ntpd
171c172
< #FREEBSD_SRC=/usr/src
---
> FREEBSD_SRC=/usr/src
188c189
< #WORKDIR=${TOPDIR}/work
---
> WORKDIR=${TOPDIR}/work
194c195
< #IMG=${WORKDIR}/FreeBSD-${KERNCONF}.img
---
> IMG=${WORKDIR}/FreeBSD-${KERNCONF}.img
202c203
< #FREEBSD_INSTALL_WORLD=y
---
> FREEBSD_INSTALL_WORLD=y
```


Now you'll need to build it as root:

    sudo ./crochet.sh -c rp2.cfg

This takes a while, so it's a fine time to make a pot of coffee, possibly starting from green beans.

## Installing the Build

Unless you're lucky enough to be running your FreeBSD system on metal, you'll need to transfer it from your vm to your local machine.  Your mechanism will be specific to your situation.  I did:

    scp clay@freebsd-vm:/root/crochet/FreeBSD-RPI2.img .
  
The next step is to burn the SD card image.  From my Linux machine I used the *dd* utility.  It's worth keeping in mind that Linux does not have as nice a dd utility as FreeBSD does, so the command won't work the same way as the FreeBSD command referenced in the wiki page.  You'll need to be aware of what to do for your own operating system.  This is what I did:

    sudo dd if=FreeBSD-RPI2.img of=/dev/mmcblk0 bs=4096

## Running It

That's it, you've got it.  Just install it in your RPi, power it up and enjoy.

By default FreeBSD doesn't bring up your network connection automatically.  The mechanism for bringing it up depends on your network situation.  Your best bet is to match your network situation to the instructions in the excellent [FreeBSD Handbook](https://www.freebsd.org/doc/handbook/network-wireless.html).
  
If you have a wireless card, you'll need to do a little google search to find the right configuration.  I brought mine up by putting the following two lines in ''/boot/loader.conf'' and restarting the computer.

    if_urtwn_load="YES"
    legal.realtek.license_ack=1
  
This brings up the kernel module for the popular EDIMax USB wireless dongles.  These seem to be the most popular of the dongles I see with the Raspberry Pi.  If you have a different dongle, google is your friend.

I then brought the wireless up with the following command:

    ifconfig wlan create wlandev urtwn0 ssid FBISurveilanceVan up

FBISurveilanceVan is the SSID for my home network.  That caused a little blue light to start blinking on the wireless device.  There's no network connection yet though.  For that I had to follow the instructions on the above linked wireless network page.

```
network={
        ssid="FBISurveilanceVan"
        psk="SomebodyWatchingMe"
}
```

I added these lines to ''/etc/rc.conf'':

```
wlans_urtwn0="wlan0"
ifconfig_wlan0="WPA SYNCDHCP"
```

I restarted the system and my wireless networking came out right away.
