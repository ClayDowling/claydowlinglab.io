---
title: "I am an Imposter"
---

Imposter syndrome, that feeling that you're really not good enough to be where you are, that you're going to be discovered and kicked out, is a women's issue.  No Real Man&trade; would ever admit to being an imposter.  Fortunately, after 20 years as a developer, I was able to woman up and admit that I too had imposter syndrome.

That admission was one of the most freeing things I have done in my life.  Years of fixing bugs in code written by people with Ph.D.s didn't do it.  A track record of delivering software that allowed companies to expand jobs didn't do it.  Accepting that I don't have all the answers, and the realization that neither does anybody else, that gave me the freedom.

> I’ve been able to work for so long because I think, ‘Next time, I’ll make something good.' &mdash; _Something Like an Autobiography_, Akira Kirosawa

I'm not alone in this.  Akira Kirosawa, arguably one of the greatest film makers to ever live, suffered from imposter syndrome.  Even if you're never seen a Japanese movie, his influence is everywhere in western cinema and television.  If he felt like he was an imposter, there's little hope that I, a self taught software developer, won't feel like one.

So embrace it.  Accept that you don't have the answer to everything.  Admit your ignorance and ask for help.  When you do that, it opens up a world of new opportunities.  Two off the top of my head:

1. You get to let somebody else be a hero, because they know a thing that can help somebody out.  There's precious few opportunities to feel like that in this world, so providing it for somebody else is a nice thing.

1. You open yourself up for exciting new discoveries.  It's lovely that, even after 20+ years as a software developer, I find new things to learn.  Just today I learned an exciting new solution for _Conway's Game of Life_ that delighted me.

Hopefully you too can embrace your status as an imperfect person, and reap the rewards.