---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
images:
- /images/claydowling-logo.png
draft: true
productid: {{ .Name }}
price: 1.42
categories:
- Product
- Workshop
---
