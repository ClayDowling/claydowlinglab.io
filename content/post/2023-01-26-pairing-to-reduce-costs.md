---
title: "Pairing to Reduce Costs"
description: "A business case for pair programming, and avoiding the trap of minimizing costs above all else."
date: 2023-01-26T11:47:01-05:00
draft: false
---

 
In spite of the reputation of programmers as anti-social loners, I adore pair programming. Some of that is because programming can be lonely. Mostly, I spend a lot less time feeling stupid when I am paired. It feels good to be less stupid.

<!--more-->

When I dug into academic research on pair programming, what I learned is that it’s not just a feeling. I’m actually less stupid when I’m paired. [Kim Man Liu and Keith C.C. Chan](https://www.cs.utexas.edu/users/mckinley/305j/pair-hcs-2006.pdf) had developers of varying experience levels solve the same problem multiple times. Some opted to pair, and some opted to work solo. What they found is that when the problem space is new, the paired team generally delivered a solution in two thirds the time as a solo developer. As the teams’ familiarity with the problem increased, the differences disappeared. By the fourth time the teams solved the same problem, there was no advantage to pair programming.

This tracks with my own experience as a software consultant. When coming on to a new project, the team knows nothing. Solo exploration is painful, but when we work as pairs, or an ensemble, we find solutions much more quickly. When we’re more familiar with the project, we often find situations where we’re solving a series of problems we’ve seen before. When we find those spots, we often stop pairing, split the work, and tackle it solo.

## When To Use Pairing

Based on the Liu & Chan study, if you look strictly at hours paid for feature delivered, you should never have developers pair. The best outcome they achieved while pairing was delivering in two thirds the time of a solo developer. Assuming all devs are paid equally, they paid 1.5 times what they should have paid.

The catch is that dollar per feature is not the only important metric to track. When we start something new, no matter how much planning we have done, we don't know if what we’ve planned is the right thing. We need to see it and use it before we’ll know for sure. Until we have something in hand, we don’t know if we’re paying people to develop the right thing, so the dollars per feature isn't important.

The first thing we need to do is ensure that we’re building the right thing. That means there’s more advantage to getting the new thing in our hands faster. Until we know we’re building the right thing, pay the higher dollar per feature of pairing. Until you are building the right thing, all of the money you have spent is spent on the wrong thing.

When should you stop pairing? When the work is well understood, and doesn’t require any special insight to complete. At that point, the work is less about thinking and more about typing. The good news is that pairing is unrewarding in these situations, and most developers will naturally revert to solo work on their own.

There’s a trap hidden in that criteria. There’s a strong desire to reduce the dollar per feature. Even at the best price per feature, developers are expensive typists. In designing our work, we should minimize the time where we’re just typing, and apply ourselves to making that rote work faster and simpler.

As an example, on one particular project, writing validation software for ECUs, the team was able to turn out half a dozen validation tests in a day. A team member noticed which parts of the validation routines were unique, and which were unique to each test. They quickly conferred with the other team members, and developed a set of simple functions to express the validations. The production went from half a dozen per day to half a dozen per hour.

## Conclusions

For software to be effective, it needs to deliver the right features, when it’s needed. This means that even though paired programming incurs higher up front costs, it has a better chance of delivering useful software. Because it gives us faster feedback, 

For pairing to be an effective strategy, it needs to be used appropriately. There is limited value in pairing when the code to be written doesn’t require any problem solving. 

