+++
title = "Makefile Dependency Management"
Description = "A simple way to manage dependencies with makefiles."
Tags = ["Development", "C"]
Categories = ["Development", "C"]
date = 2022-10-25
+++

I was running an impromptu workshop yesterday and hit upon an excellent way to manage dependencies in a Makefile.  This mechanism downloads dependencies and keeps them updated with nothing more than standard make and git.

<!--more-->

```Makefile
.PHONY: test-setup test all bin

bin:
    $(MAKE) -C src

test: test-setup
    $(MAKE) -C test

test-setup: current-bdd-for-c current-fff

current-bdd-for-c: bdd-for-c
    cd bdd-for-c; git pull

bdd-for-c:
    git clone https://github.com/grassator/bdd-for-c.git

current-fff: fff
    cd fff; git pull

fff:
    git clone https://github.com/meekrosoft/fff.git
```

The targets which perform the repository clones must match the name of the generated folder, because these are real targets that will be used by other targets.

If you need to stick to a particular version of a library, it's straight forward to check out a specific tag or hash code using the normal git command line options.
