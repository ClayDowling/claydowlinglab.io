---
title: "An Atmel CI/CD Pipeline"
description: "Automate builds and tests"
date: 2020-07-03T16:27:22-04:00
images:
- /images/claydowling-logo.png
draft: true
---

## The Components

- Setting up build infrastructure.
- Building tests.
- Building the target binary.
- Flashing to the device.
- Itegration testing

## Build Infrastructure

Need to get a build agent set up locally.

