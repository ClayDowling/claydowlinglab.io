---
title: "You Got SQL In My ORM"
description: "A free workshop for leveling up your java database skills"
date: 2022-03-17
tags:
  - development
  - java
  - database
---

Object Relational Managers like Hibernate or Entity Framework simplify
life for developers who use databases.  Unfortunately they also make it
easy to do things that will cause us problems down the road, that won't
show up until the application has been in production for a while.

Using Spring Boot and Hibernate, we'll work through examples as a group
of how to use native SQL with Hibernate to remove those bottlenecks.

Session begins at 9am Eastern and runs through noon on April 2, 2022.
Sign Up to be added to the calendar invite:

April 2, 2022: [You Got SQL In My ORM](https://forms.gle/2pxLwS6uATtDEZEx5)
