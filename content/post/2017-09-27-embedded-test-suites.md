---
title: "Embedded Test Suites"
date: 2017-09-27
tags:
 - programming
 - test
---

At [Prairie.Code()](https://prairiecode.amegala.com/) I provided an [example embedded project](https://github.com/LazarusID/pcaw) to demonstrate test driven development as part of an Arduino workshop.  The example I gave used the Unity testing framework, but there are several others and they're worth exploring.

## Unity

http://www.throwtheswitch.org/unity

I love Unity for its compactness, and how easy it is to use.  It's small
enough to include the complete source in your project without bloating it.
The examples in my presentations were all written with Unity.

The site ThrowTheSwitch.org is also generally fun reading if you enjoy
embedded development.  The authors seem like they really enjoy their work,
and want to make embedded development easy and fun for everyone.

## Google Test

https://github.com/google/googletest

This is a full-featured testing and mocking suite.  It's fairly large, but
it has a lot of advanced testing and mocking features that make working on
complex projects easier.  This is without a doubt my go to test framework
when I am writing C and C++ code.  It also intgrates well with common IDEs
and Continuous Integration systems.

## check

https://libcheck.github.io/check/

This library is a good half way point between Unity and Google Test.  It
installs as a separate library on your system.  It is intended to mesh well
with Autotools builds, but it is easy to integrate into any build.  For a
pure C project, this is rapidly becoming my favorite testing suite.  It has
an easy to follow syntax, good support for complicated test suites, and nice
output for test failures.

## Boost::Test

Part of the Boost library for C++, the Test library gets lots of happy reviews from users.  I really like the Boost libraries in general.  Even if you don't use the Test library, you should look at Boost for all of the great libraries that can make your work easier and more fun.

If you're already incorporating Boost in your project, this probably isn't a terrible choice.  But the documentation is difficult to decipher and I'd prefer nicer error output that showed actual values being compared rather than just the expression tested.