---
title: "C++11 Array Iteration"
date: 2016-01-29
tags:
 - programming
---

I've just gotten to play with the C++11 ''for'' extensions for the first time.  It's really cool, especially the ability to iterate over a plain old array.  Dig this code from my current hobby project:

```C++
int Driver::getSpeedBonus(int speed)
{
    struct speedbonus {
        int speed;
        int bonus;
    };

    struct speedbonus benefits[] = {
    {200, 10},
    {140, 9},
    {100, 8},
    {60, 7},
    {40, 6},
    {30, 5},
    {20, 4},
    {12, 3},
    {10, 2},
    {0, 0}
    };

    for(struct speedbonus b : benefits) {
        if (speed >= b.speed) {
            return b.bonus;
        }
    }

}
```

It's a nice feature that other languages have enjoyed for years now.  Of course, I'm also a little behind on my C++ learning.