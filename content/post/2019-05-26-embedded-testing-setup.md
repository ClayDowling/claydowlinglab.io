+++
author = ""
comments = true	# set false to hide Disqus
date = "2019-05-26T08:30:14-04:00"
image = "/images/cover/computer-and-martini.jpg"
menu = ""		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
slug = "embedded-testing-setup"
tags = ["embedded","testing"]
title = "Embedded Testing Setup"
+++

One of the most confusing parts of setting your workstation up for embedded testing is just what tools you'll need and how to install them.  Fortunately the number of tools you'll need is minimal.  You need:

* A compiler that builds native binaries for your workstation operating system.  Unless you want a very hard time, its command line should be compatible with the UNIX cc command line.  Gcc and clang both conform to this interface, and they are available for most common workstation operating systems.
* A version of GNU Make.  Other makes, such as BSD make, can work, but I'm going to take advantage of GNU Make features to simplify our lives.
* A good programming text editor.  The venerable vi and emacs are fine choices if you are comfortable with them, otherwise I strongly recommend Atom or VS Code with appropriate C/C++ plugins.
* A good terminal.
* Git for version control.  I won't dwell heavily on version control, except to say that you should be using it.  Any assumptions I make about version control will assume git.

Let's look at the options for each operating system

## Windows

First, install [Chocolatey](https://chocolatey.org/).  You can install all of these tools manually, but it will take a lot longer and be a lot more work.

* posh-git: This will also install git.  This is not necessary, but it will make your life a lot easier.
* make: This installs GNU Make.
* llvm: Installs the llvm family of compilers, which includes clang, clang++, and a modern set of standard libraries.
* conemu: The best console emulator for Windows.  There are a variety of color schemes for it which you can adjust to suit your own needs.

Please don't try to use the default cmd.exe terminal.  Powershell or GitBash are available on your machine, and you owe it to yourself not to handicap yourself with bad tools.


## OSX

* A terminal.  The default terminal is workable, but over the long haul I have found iTerm2 to be a better choice.
* x-code command line tools.  The easiest way to install this is to type "gcc --version" on the command line.  If you have the command line tools installed, you'll get information about your compiler version.  If you don't have it installed, you'll be prompted to install the tools.

## Unix

Most Unix based systems will have the necessary tools installed already.  To see if you have a compiler installed, try the following at a command prompt:

    cc --version

If the response is a software banner with a version, you have what you need installed.  Otherwise, you need to install the build essentials for your system.  On a Debian based system like Ubuntu, that would be

    apt install build-essential
