---
title: "Your New Remote Work Life"
date: 2020-03-07
---

A lot of people are discovering what it's like to work remote suddenly, or they're about to.  It's possible that this experience has sucked.  I worked for over a year on mixed remote and collocated teams, and there were some important lessons we learned.  My team used extremem programming practices, which are intensely collaborative, including pair programming and test first code.  Here are some of the things we learned.

## Tools

1. The Microphone Is Everything.  You don't need expensive noise canceling headphones.  In fact they'll probably be undesirable.  But a highly directional microphone is essential.  When you are present in a room you can easily filter out background noise.  When you're hearing a room over a remote connection, background noise and the person you are speaking with are heard at roughly the same level.  My team settled on [Jabra Evolve 65 headsets](https://amzn.to/2wzD6JQ) after a lot of trial and error with inexpensive gaming headsets.

1. A low overhead virtual meeting tool.  Our best luck was with Google Meet, because it doesn't require any software installation, has competent presentation, and supports chat when necessary.  Zoom was acceptable, and it was better for larger meetings, but our daily needs were around small, low overhead conversations between collaborators.  If your company uses Microsoft Teams and you already have it set up, we've found it to be workable, but it's not the choice I would recommend if you don't have something already.

1. If there are any people who are colocated, a puck speaker like the [Jabra Speak 510](https://amzn.to/3aAnK6t) should be used for any meetings.  Even the best conference room speakers don't pick up very well, so it's a good idea for whoever is speaking to be within 3 or 4 feet of the speaker.

1. A good chat app.  I've been using Slack for the last four years and I like it for the ease of setting it up.  It's also easy to install on a phone.  My current team is spread across two countries, so having the app on our phones lets us coordinate our sprawling team easily.  I've also used Internet Relay Chat with great success.  IRC requires more technical support than Slack, but also puts your important conversations under your own control.  [Google Chat](http://chat.google.com) also looks promising, and blends nicely with the rest of the Google office suite.

## Collaboration Tools

### Office Suite

There's a good chance that your company has already settled on their office suite.  If they haven't, I've used both Microsoft Office 365 and the Google Suite.  The Google Suite is optimized for the remote and nomadic worker, and requires no software installation.  Remote collaboration is automatic, and remote collaboration is going to be essential for a distributed team.  How much do I believe in the Google suite?  I bought a plan for my family, because $50/year/person is worth the headache I don't have from supporting Office or some other set of tools.

Only use the Microsoft tools if your corporate overlords force it on you.  If you're remote, dealing with a PC support issue because your presentation tool or spreadsheet stopped working is going to be difficult.

### Software Development

We do paired, test first development.  We're constantly switching who has control of the keyboard.  Our hands-down winner has been Visual Studio Code and their Live Share extension. One person in the collaboration session hosts the Live Share session, inviting people in by sharing a web link via chat.  Everybody has access to the editor, the file tree, and the terminal.

If you have team members who aren't familiar with using the terminal for common bulid tasks, this is the time to learn.  The good news here is that most modern development tools are fairly command line friendly.

We haven't had good luck with full blown IDEs.  I love the JetBrains tools, but they're miserable for remote collaboration.  Visual Studio is slightly better because it does have Live Share, but it takes some tweaking to get the terminal to work well, and you'll still have a tendency to revert to the GUI.  Especially when it comes to running tests, that is isolating to the remote people, so I don't recommend Visual Studio.

## Logistics

### Standing Meeting Rooms

First, set up some dedicated meeting spaces in your video or audio conferencing tool.  We set up standing Google Meet rooms and pinned their URLs in our team's slack channel.  With one room for each pair, it was easy for the pairs to get going when they start their day.  When you need an inpromptu conversation with a team member, you can grab one of the available rooms.  If setting up these conversations takes a lot of overhead, you won't have the conversations that you need to have, and work is going to stop being fun.

### If One Person is Remote, We're All Remote

If you want to keep the team dynamic healthy, you don't want remote people feeling excluded.  Don't call all of the colocated team into a conference room without settingn up a session with the remote people.  You can either do that with a speaker like the Jabra 510 I mentioned above, or by everybody joining one of the shared meeting rooms.

It's important for the colocated folks in the office to protect their remote team members.  If outsiders come in with information that's important to the team, be sure they're included.  The folks in the office need to be advocates for the folks out of the office.

### Establish Core Hours

Because of the intensely collaborative way we work, we can't be as asynchronous as is ideal for a fully remote team.  One team member liked to get up at 5 so he could swim at his gym, when there wasn't intense competition for lanes.  Starting his coding day immediately after was less fine, when his pair partner might have an hour+ drive to get to the client site.  Especially because it was easier to swim at my gym around 6:30, and then I'd have to drive.

If there is an expectation that people be available between certain hours, and that activities requiring collaboration needed to occur during those hours, it saves a lot of friction.  If your team doesn't do pair programming, this might be less of an issue, but we found it to be very important.

### Introduce Your Pets

Your animals will participate in meetings and your pairing sessions.  It's worth the trouble to introduce someone who will be involved in your working day this much.  My pair partners got used to my cat watching us code judgementally.  We all got used to another team member's cat chiming in on the daily stand up.

## Summary

I hope this points make your remote work life easier.  It can definitely suck at first, but once you iron out the hard parts, it's actually okay.
