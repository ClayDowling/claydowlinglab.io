---
title: "Halloween Video Display"
date: 2016-07-05
---

Halloween is kind of a big deal where I live, so this year I decided I needed a nice rotation of videos projected onto my garage wall.  There are a few things you'll need:

- A computer, preferably a laptop or netbook.
- A [projector.]("http://www.amazon.com/s/ref=as_li_ss_tl?_encoding=UTF8&camp=1789&creative=390957&field-keywords=projector&linkCode=ur2&tag=lazarusintern-20&url=search-alias%3Daps&linkId=K4QCZYZS6MFQCYFK)
- External speakers, ideally weather proof.
- Extension cord
- Appropriate Halloween videos
- A safe place out of the weather to set up your gear

## Video

The fun part of this project is putting together the video.  I plunked down a bunch of money at the local Halloween store to get a DVD full of silhouette videos that were Halloween themed.  The videos were a hit, but that was kind of expensive.

I had more fun pulling spooky videos off YouTube.  I love classical music, and there's some really scary stuff out there.  My favorites that I pulled:

{{< youtube VPmpDD3MGas >}}
{{< youtube SLCuL-K39eQ >}}
{{< youtube wU1NGA5Vykg >}}
{{< youtube 5cPOHXpk3VI >}}

You might want to edit off silent credit bits on your videos, because nobody is going to want to read that on your display, and there's a good chance they won't even be able to read it.  I like OpenShot for my video editing, but use whatever is fast and easy on your computer.

Put all of these videos in a single folder on your computer.

## Playback

The tool that's going to make you look like a genius is [VLC](http://www.videolan.org/vlc/index.html).  It works on every common platform and it has exactly one job: playing media.  It does that very well and stays out of your way.

- Open a Folder (or Directory).
- Select Random Playback (crossing streams button at the bottom of the screen)
- Select Repeat (loop button at bottom of the screen)
- Select full screen.

I found it worked best to run my laptop in Mirror mode, because when I was setting it up it was daylight, and I really couldn't read the projector.

## Setup

The first consideration in your setup that you want to consider is putting your projector into a place where it will be protected from the weather, and if it needs to be left unattended, out of the reach of the more acquisitive members of the community.

If you don't have your projector yet, it might be worth investing in a smaller one.  You also don't need to worry about video resolution to seriously.  By the time you've projected it through a window and onto a building, you will have lost most of your video fidelity.  Especially if it rains.

You also want to test out your full setup inside, under controlled circumstances.  There are a lot of little details that need to be worked out.

Once you have your technology working, you also need to work out how it's going to work in your outdoor setup.  Consider the following items:

- Getting power to the setup.
- Setting up the projector so that it projects onto a wall.
- Getting sound out.

My setup was in my truck, projected onto the front of my garage.  I ran a long heavy extension cord from the garage and in through the back slider of my truck.  That connected to a power strip, which in turn powered my computer and the projector.  In your own setup, consider that there are going to be a lot of children in masks that can't see well and are high on sugar.  Keeping them from running headlong into trees is a challenge.  They're pretty much assured to trip over a cord left lying across a path.

I put a board between the front seat back and the dash in my truck, and set the projector on the board.  There was a wicked distortion projecting it onto the front of my garage, but for a first effort I decided it was acceptable.  Many projectors have adjustments to compensate for this.  Mine was a cut rate projector, so I coped.

The idea sound solution would be a set of outdoor speakers.  Those are really expensive, and might not be appropriate for your situation.  I tried a couple of old computer speakers, but found that time had not been kind.  I ultimately settled on a $30 Sound Cube that I picked up at Menards.  It put out a good volume of sound, and combined with a 6' 3.5mm extension cable and a matching micro USB cable I was able to put it outside of my truck and run for hours.
I put my speaker under my truck.  The sound cube nestled nicely into the frame of my truck, where it was protected from weather and couldn't be seen by people who might be looking for a free bluetooth speaker.

## Results

There were big crowds stopping in front of my house all night.  Even I had to step out front to watch it occasionally.
