+++
Title = "Boozy Gummies"
Description = "A fun comfort food that combines your childhood with the hazzier parts of your early 20s"
Date = 2020-04-18
Tags = ["food"]
+++

I've been experimenting with booze-filled gummies for a while now, and I've
been asked by several people to share my recipe.  We're all stuck and home and
living with a lot of stress, so a comfort food like finger jello goes a long
way to improving happiness.

## Tools

First, let's start with the molds.  You need silicone molds of some sort to
hold and form the gelatin.  The gummies we're making are significantly stiffer
than the finger jello you might have known as a child, so cutting up a sheet
pan into squares isn't going to be a good time.  I'm using simple ice cube and
cookie molds I picked up at my grocery store over the years, but there's plenty
of fun shapes in the [Amazon Bakeware](https://amzn.to/3ez3LI3).

Three other important things you'll need:

  - Instant read thermometer that's accurate between 100 and 120 degrees.
    [Grilling thermometers](https://amzn.to/3bmB1Ar) work especially well for
    this.
  - Heat proof cup with a pour spout.  I use a [2 cup pyrex measuring
    cup](https://amzn.to/3cqal1V).
  - A wire rack or baking sheet to support the mold.

## Ingredients

  - 5 packets of plain gelatin. This works out to roughly 40g total if you don't have Knox packets.
  - 1 cup booze.
  - 1/2 cup sugar.  *This is not optional.* Without sugar you won't be able to taste your booze, and you will hate it.

## Preparation

  - Place molds on the cookie sheet or wire rack.
  - Pour the booze first into a medium sauce pan, then add the other ingredients.
  - Over low heat, stir constantly to dissolve.  Do not heat over 120 degrees or your booze will cook off.
  - Pour into your pouring container (i.e. the pyrex).
  - Pour into molds.
  - Put the mold, on the rack, in the refrigerator for a couple of hours to set.
  - Pop your boozy gummies out of the mold and store in a plastic bag or sealed container.  

Regular gummie candies have some other ingredients (mostly modified starches)
to help them retain moisture.  These pure gelatin gummies will dry out and
harden rapidly if left in the open air.  Leaving them overnight to chill yields
something like the texture of boot leather.

### Experimentation

There are a lot of different ways to experiment with this.  I haven't tried any
gin drinks.  I do know that you shouldn't waste your top or mid shelf liquor on
this project.  The gelatin and sugar will mask the subtle notes of a fine
whiskey.  The sugar is necessary to allow you to taste the liquor, otherwise
the gelatin keeps the flavor locked away from your tongue.

You can definitely use flavored gelatins for this.  We found a packet of
raspberry gelatin with the sugar already added and combined it with vodka.

The basic recipe for success is:

  - 500ml of booze
  - 40g of gelatin
  - 40g of sugar

If you use a flavored gelatin with the sugar already added, the easy way to
figure out how much of each ingredient the packet contributes is to look at the
nutrition panel.  Flavored gelatin has exactly two ingredients, and the
nutrition panel has exactly two items.

  - Each gram of protien is a gram of gelatin.
  - Each gram of carbohydrate is a gram of sugar.

Add your packet, then add enough of gelatin and sugar to bring the totals to 40g each.

## Conclusion

These are fun to make, and may bring you back to happy memories of your youth,
stumbling home in the small hours.  Also, if you're stuck on an endless remote
conference call, nobody can tell that the gummies you're eating are 80 proof.
