---
title: "Haunted Pumpkin Kit"
description: "Hardware kit for the Haunted Pumpkin"
date: 2020-11-13T17:59:02-05:00
images:
- /images/haunted-pumpkin-board.jpg
productid: haunted-pumpkin-kit
price: 15.00
weight: 5
categories:
- Product
---

{{< figure src="/images/cover/haunted-pumpkin-board.jpg" title="Completed Circuit Board" >}}

Build your own Haunted Pumpkin.

Contains the following parts:

{{< figure src="/images/haunted-pumpkin-parts.png" link="/images/haunted-pumpkin-parts.png" target="_new" title="Haunted Pumpkin Parts" >}}