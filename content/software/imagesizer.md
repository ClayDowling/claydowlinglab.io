+++
comments = true	# set false to hide Disqus
date = "2019-07-06T22:04:23-04:00"
draft = true
image = ""
menu = "main"		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
title = "Image Sizer"
+++

## Download

* [Windows](/software/imagesizer-1.3-win.zip)
* [Mac](/software/imagesizer-1.3-osx.zip)