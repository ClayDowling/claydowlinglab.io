---
title: "Business Reasons"
---

## Business Reasons

* A small language &rArr; fewer options
* Fewer options &rArr; Focus on getting stuff done
* Getting Stuff Done &rArr; Happy Developers
