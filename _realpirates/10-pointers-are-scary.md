---
title: "Pointers are Scary"
---

# Pointers are Scary

<blockquote>
    &ldquo;Programming is like playing with a loaded gun. C cocks the hammer for you.&rdquo;
    <footer>
        <cite>&mdash;Anonymous IBM Employee</cite>
    </footer>
</blockquote>