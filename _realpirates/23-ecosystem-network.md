---
title: "Network Client"
---

## Network/REST Client

### libcurl

If it's a network protocol, libcurl speaks it.

Easy to mock/test code using libcurl.