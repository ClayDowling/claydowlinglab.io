---
title: "Test Are Part of Build"
---

## Make Tests Part of Your Build

    all: test product

    product: $(OBJECTS) main.o
        $(CC) -o $@ $^
        
    test: test-product
        ./test-product
        valgrind test-product

    test-product: $(TEST_OBJECTS) $(OBJECTS)
        $(CC) -o $@ $^ $(TEST_LIBS)
