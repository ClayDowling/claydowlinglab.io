---
title: "Symmetry Is Your Friend"
---

## Symmetry Is Your Friend

| If You Write...  | Also Write...    |
| ===============  | =============    |
| malloc/calloc    | free             |
| myobject_create  | myobject_destroy |
