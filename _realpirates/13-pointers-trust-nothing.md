---
title: "Trust Nothing"
---

## Trust Nothing

Write automated tests for all of your code

Use valgrind on your tests

Do not ignore valgrind errors