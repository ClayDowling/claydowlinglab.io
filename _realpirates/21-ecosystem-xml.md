---
title: "XML"
---

## XML

* _libxml2_ - Fast parser using document or sax style.
* _xpat_ - Light weight sax parser.
* _Apache Xerces_ - __Do Not Use__
* _Microsoft XML_ - __Do Not Use__
