---
title: "Test Example"
---

## Sample Test

    START_TEST(skillcmp_givenAandB_returnsNegativeOne) {
        skill_t *a = skill_create("A", 0, S_CATEGORY, S_ATTRIBUTE);
        skill_t *b = skill_create("B", 0, S_CATEGORY, S_ATTRIBUTE);
        
        ck_assert_int_eq(-1, skill_cmp(a, b));
        
        skill_destroy(a);
        skill_destroy(b);
    }
    END_TEST
