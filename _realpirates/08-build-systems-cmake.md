---
title:  "CMake"
---

## CMake

    cmake_minimum_required(VERSION 2.8)

    file(GLOB program_SRC
        "*.c"
    )

    add_executable(myprogram ${program_SRC})