---
title: "Take Action"
---

## Take Action

- Hold each other accountable.
- Bring concerns to stakeholders.
- Hold the stakeholders accountable.