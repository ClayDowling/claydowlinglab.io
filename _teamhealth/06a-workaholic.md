---
title: "Workaholism"
---

## Workaholism

1. You think of how you can free up more time to work.
1. You spend much more time working than initially intended.
1. You work in order to reduce feelings of guilt, anxiety, helplessness or depression.
1. You have been told by others to cut down on work without listening to them.
1. You become stressed if you are prohibited from working.
1. You deprioritize hobbies, leisure activities, and/or exercise because of your work.
1. You work so much that it has negatively influenced your health.

<cite>University of Bergen. (2016, May 25). [Workaholism tied to psychiatric disorders](http://www.sciencedaily.com/releases/2016/05/160525084547.htm). ScienceDaily. Retrieved April 13, 2019</cite>