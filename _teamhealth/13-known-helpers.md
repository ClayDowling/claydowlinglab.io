---
title: "Known Helpers"
---

## What Does the Science Say?

- Connect the work to the consumer.
- Keep tasks small.

&mdash; "Intrinsic Motivation at Work: What really drives employee engagement." Kenneth W. Thomas.