---
title: "Work Life Balance"
---

## Work is not Life

![Thea Disapproves of My Code](/assets/images/slides/thea-watching-me-code.jpg)

- Have a place to go away from work.
- Make sure your cat knows that you live there.
