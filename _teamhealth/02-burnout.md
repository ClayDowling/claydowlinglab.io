---
title: "Burnout Rate"
---

# 57%

[Percentage of workers reporting job burnout](http://blog.teamblind.com/index.php/2018/05/29/close-to-60-percent-of-surveyed-tech-workers-are-burnt-out-credit-karma-tops-the-list-for-most-employees-suffering-from-burnout/).