---
title: "Burnout is Expensive"
---
## Burnout is Expensive

[$125M - $190M a year in medical expenses](https://hbswk.hbs.edu/item/national-health-costs-could-decrease-if-managers-reduce-work-stress)