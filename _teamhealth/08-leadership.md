---
title: "You Are a Leader"
---

# You Are a Leader

<aside class="notes">

Leadership doesn't come with a title.  Leadership is how you help other people.

</aside>