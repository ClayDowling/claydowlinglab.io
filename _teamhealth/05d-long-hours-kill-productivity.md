---
title: "Long Hours Kill Productivity"
---

> Ernst Abbe, the head of one of the greatest German factories, wrote many years ago that the shortening from nine to eight hours, that is, a cutting-down of more than 10 per cent, did not involve a reduction of the day's product, but an increase

<cite>Hugo Muensterberg, "Psychology and Industrial Efficiency" (1913)</cite>