---
title: "Understand the Problem"
---

## Talk to Team Mates

- Are they happy?
- Are they being heard?
- What would make the job better?