---
title: "Long Hours Kill"
---

## Long Hours Kill

Working 55 hours or more per week:

- 13% more likely to have a heart attack
- 33% more likely to have a stroke

<cite>Harvard Health Blog, [Only the overworked die young](https://www.health.harvard.edu/blog/only-the-overworked-die-young-201512148815)</cite>